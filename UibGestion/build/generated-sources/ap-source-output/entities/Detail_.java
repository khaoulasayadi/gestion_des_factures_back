package entities;

import entities.BcDsiProjet;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-10T22:05:10")
@StaticMetamodel(Detail.class)
public class Detail_ { 

    public static volatile SingularAttribute<Detail, Integer> iddetail;
    public static volatile SingularAttribute<Detail, Float> taxes;
    public static volatile SingularAttribute<Detail, Float> pUIBrut;
    public static volatile SingularAttribute<Detail, Float> pUNet;
    public static volatile SingularAttribute<Detail, Float> remise;
    public static volatile SingularAttribute<Detail, String> type;
    public static volatile SingularAttribute<Detail, Float> frais;
    public static volatile SingularAttribute<Detail, Integer> codearticle;
    public static volatile SingularAttribute<Detail, Float> totalHT;
    public static volatile SingularAttribute<Detail, BcDsiProjet> dsiiFK;
    public static volatile SingularAttribute<Detail, String> element;
    public static volatile SingularAttribute<Detail, Integer> quantite;

}