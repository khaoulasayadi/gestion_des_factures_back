package entities;

import entities.BcDsiProjet;
import entities.Budget;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-10T22:05:10")
@StaticMetamodel(Projet.class)
public class Projet_ { 

    public static volatile SingularAttribute<Projet, String> piecesjointes;
    public static volatile SingularAttribute<Projet, String> responsable;
    public static volatile ListAttribute<Projet, BcDsiProjet> bcDsiProjetList;
    public static volatile SingularAttribute<Projet, Date> datedebut;
    public static volatile ListAttribute<Projet, Budget> budgetList;
    public static volatile SingularAttribute<Projet, String> description;
    public static volatile SingularAttribute<Projet, Integer> refprojet;
    public static volatile SingularAttribute<Projet, Date> datefin;
    public static volatile SingularAttribute<Projet, String> commentaires;
    public static volatile SingularAttribute<Projet, String> intitule;

}