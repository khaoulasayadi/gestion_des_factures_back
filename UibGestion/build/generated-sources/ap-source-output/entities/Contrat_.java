package entities;

import entities.Facture;
import entities.Intervention;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-10T22:05:10")
@StaticMetamodel(Contrat.class)
public class Contrat_ { 

    public static volatile SingularAttribute<Contrat, String> periodedernierefact;
    public static volatile SingularAttribute<Contrat, String> piecesjointes;
    public static volatile SingularAttribute<Contrat, Date> datedernierefact;
    public static volatile SingularAttribute<Contrat, String> frs;
    public static volatile SingularAttribute<Contrat, String> libelle;
    public static volatile SingularAttribute<Contrat, Float> mTHTdernierefact;
    public static volatile SingularAttribute<Contrat, String> fournisseur;
    public static volatile SingularAttribute<Contrat, String> rtbCtb;
    public static volatile SingularAttribute<Contrat, String> type;
    public static volatile SingularAttribute<Contrat, String> augmentation;
    public static volatile SingularAttribute<Contrat, Integer> nbreinterv;
    public static volatile SingularAttribute<Contrat, Integer> reference;
    public static volatile SingularAttribute<Contrat, Float> mTcontrat;
    public static volatile ListAttribute<Contrat, Intervention> interventionList;
    public static volatile SingularAttribute<Contrat, String> rUNCHANGEbank;
    public static volatile SingularAttribute<Contrat, String> commentairesPSO;
    public static volatile SingularAttribute<Contrat, Float> montantendevises;
    public static volatile SingularAttribute<Contrat, String> commentaires;
    public static volatile SingularAttribute<Contrat, Date> datesignature;
    public static volatile ListAttribute<Contrat, Facture> factureList;
    public static volatile SingularAttribute<Contrat, String> statut;
    public static volatile SingularAttribute<Contrat, Integer> freqpaiement;

}