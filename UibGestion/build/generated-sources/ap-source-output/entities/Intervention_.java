package entities;

import entities.Contrat;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-10T22:05:10")
@StaticMetamodel(Intervention.class)
public class Intervention_ { 

    public static volatile SingularAttribute<Intervention, Date> dateintv;
    public static volatile SingularAttribute<Intervention, Float> coutannuel;
    public static volatile SingularAttribute<Intervention, Integer> idInterv;
    public static volatile SingularAttribute<Intervention, Contrat> contratFK;
    public static volatile SingularAttribute<Intervention, Integer> nbredepersonnes;
    public static volatile SingularAttribute<Intervention, Integer> duree;
    public static volatile SingularAttribute<Intervention, String> description;
    public static volatile SingularAttribute<Intervention, String> piecejointe;

}