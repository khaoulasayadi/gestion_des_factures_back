package entities;

import entities.BonCommande;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-10T22:05:10")
@StaticMetamodel(Materiel.class)
public class Materiel_ { 

    public static volatile SingularAttribute<Materiel, BonCommande> bcmFk;
    public static volatile SingularAttribute<Materiel, Integer> idmat;
    public static volatile SingularAttribute<Materiel, Float> cout;
    public static volatile SingularAttribute<Materiel, String> nomMat;

}