package entities;

import entities.BcDsiProjet;
import entities.BonCommande;
import entities.Contrat;
import entities.DetailFacture;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-10T22:05:10")
@StaticMetamodel(Facture.class)
public class Facture_ { 

    public static volatile SingularAttribute<Facture, Date> date;
    public static volatile SingularAttribute<Facture, String> prestataire;
    public static volatile SingularAttribute<Facture, Integer> codeclient;
    public static volatile SingularAttribute<Facture, BonCommande> bcFk;
    public static volatile SingularAttribute<Facture, String> fournisseur;
    public static volatile SingularAttribute<Facture, String> type;
    public static volatile SingularAttribute<Facture, Contrat> contratFK;
    public static volatile SingularAttribute<Facture, Integer> numfacture;
    public static volatile SingularAttribute<Facture, String> contact;
    public static volatile SingularAttribute<Facture, String> client;
    public static volatile SingularAttribute<Facture, BcDsiProjet> dcpFk;
    public static volatile SingularAttribute<Facture, String> emailclient;
    public static volatile SingularAttribute<Facture, String> rep;
    public static volatile SingularAttribute<Facture, Float> totalHT;
    public static volatile SingularAttribute<Facture, String> famille;
    public static volatile SingularAttribute<Facture, Float> tva;
    public static volatile SingularAttribute<Facture, Float> netapayer;
    public static volatile SingularAttribute<Facture, String> matfiscale;
    public static volatile SingularAttribute<Facture, Float> baseTVA;
    public static volatile SingularAttribute<Facture, Integer> charge;
    public static volatile SingularAttribute<Facture, String> responsable;
    public static volatile SingularAttribute<Facture, Float> montantHT;
    public static volatile SingularAttribute<Facture, Date> datecmdclient;
    public static volatile SingularAttribute<Facture, Float> taux;
    public static volatile SingularAttribute<Facture, String> domaine;
    public static volatile SingularAttribute<Facture, Float> totalTTC;
    public static volatile SingularAttribute<Facture, Float> totalTVA;
    public static volatile SingularAttribute<Facture, String> objet;
    public static volatile SingularAttribute<Facture, Float> montantTVA;
    public static volatile SingularAttribute<Facture, Date> datedefacturation;
    public static volatile SingularAttribute<Facture, Integer> numcodeclient;
    public static volatile SingularAttribute<Facture, String> modedepaiement;
    public static volatile SingularAttribute<Facture, Float> prixtotal;
    public static volatile SingularAttribute<Facture, Integer> idfacture;
    public static volatile ListAttribute<Facture, DetailFacture> detailFactureList;
    public static volatile SingularAttribute<Facture, String> emailsoc;
    public static volatile SingularAttribute<Facture, Integer> telsoc;
    public static volatile SingularAttribute<Facture, String> adresse;
    public static volatile SingularAttribute<Facture, String> cahierdecharge;
    public static volatile SingularAttribute<Facture, String> commentaires;
    public static volatile SingularAttribute<Facture, Date> datedepaiement;
    public static volatile SingularAttribute<Facture, String> timbrefiscale;
    public static volatile SingularAttribute<Facture, Float> montantfacture;

}