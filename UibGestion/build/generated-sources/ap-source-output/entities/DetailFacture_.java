package entities;

import entities.Facture;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-10T22:05:10")
@StaticMetamodel(DetailFacture.class)
public class DetailFacture_ { 

    public static volatile SingularAttribute<DetailFacture, String> pUHi;
    public static volatile SingularAttribute<DetailFacture, String> pUHt;
    public static volatile SingularAttribute<DetailFacture, Facture> factureFk;
    public static volatile SingularAttribute<DetailFacture, String> designation;
    public static volatile SingularAttribute<DetailFacture, Integer> codearticle;
    public static volatile SingularAttribute<DetailFacture, Integer> iddet;
    public static volatile SingularAttribute<DetailFacture, Integer> quantite;

}