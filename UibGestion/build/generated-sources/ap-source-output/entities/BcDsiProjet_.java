package entities;

import entities.Detail;
import entities.Facture;
import entities.Projet;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-10T22:05:10")
@StaticMetamodel(BcDsiProjet.class)
public class BcDsiProjet_ { 

    public static volatile SingularAttribute<BcDsiProjet, String> prestataire;
    public static volatile SingularAttribute<BcDsiProjet, Integer> refBc;
    public static volatile SingularAttribute<BcDsiProjet, String> piecesjointes;
    public static volatile SingularAttribute<BcDsiProjet, String> responsable;
    public static volatile SingularAttribute<BcDsiProjet, Projet> projetFK;
    public static volatile SingularAttribute<BcDsiProjet, Float> taxes;
    public static volatile SingularAttribute<BcDsiProjet, Float> frais;
    public static volatile SingularAttribute<BcDsiProjet, String> modaliteDePaiement;
    public static volatile SingularAttribute<BcDsiProjet, Float> mnttttalTTC;
    public static volatile SingularAttribute<BcDsiProjet, Float> mnttttalHT;
    public static volatile ListAttribute<BcDsiProjet, Detail> detailList;
    public static volatile SingularAttribute<BcDsiProjet, Date> datesignature;
    public static volatile SingularAttribute<BcDsiProjet, String> commentaire;
    public static volatile ListAttribute<BcDsiProjet, Facture> factureList;

}