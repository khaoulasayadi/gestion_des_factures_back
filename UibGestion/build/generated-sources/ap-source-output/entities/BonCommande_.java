package entities;

import entities.Facture;
import entities.Materiel;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-10T22:05:10")
@StaticMetamodel(BonCommande.class)
public class BonCommande_ { 

    public static volatile SingularAttribute<BonCommande, String> prestataire;
    public static volatile SingularAttribute<BonCommande, Integer> refBC;
    public static volatile SingularAttribute<BonCommande, Float> coutTTC;
    public static volatile SingularAttribute<BonCommande, String> piecesjointes;
    public static volatile SingularAttribute<BonCommande, String> responsable;
    public static volatile SingularAttribute<BonCommande, Float> coutHT;
    public static volatile SingularAttribute<BonCommande, String> commentaires;
    public static volatile ListAttribute<BonCommande, Materiel> matérielList;
    public static volatile SingularAttribute<BonCommande, Date> datesignature;
    public static volatile ListAttribute<BonCommande, Facture> factureList;
    public static volatile SingularAttribute<BonCommande, Date> dateintervention;

}