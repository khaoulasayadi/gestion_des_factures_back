/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Sana
 */
@Entity
@Table(name = "budget")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Budget.findAll", query = "SELECT b FROM Budget b")
    , @NamedQuery(name = "Budget.findByIdbudget", query = "SELECT b FROM Budget b WHERE b.idbudget = :idbudget")
    , @NamedQuery(name = "Budget.findByAnnee", query = "SELECT b FROM Budget b WHERE b.annee = :annee")
    , @NamedQuery(name = "Budget.findByBinitial", query = "SELECT b FROM Budget b WHERE b.binitial = :binitial")
    , @NamedQuery(name = "Budget.findByBreestime", query = "SELECT b FROM Budget b WHERE b.breestime = :breestime")
    , @NamedQuery(name = "Budget.findByBengage", query = "SELECT b FROM Budget b WHERE b.bengage = :bengage")
    , @NamedQuery(name = "Budget.findByProjet", query = "SELECT b FROM Budget b WHERE b.projettFK.refprojet = :projettFK")
    , @NamedQuery(name = "Budget.findByBrealise", query = "SELECT b FROM Budget b WHERE b.brealise = :brealise")})
public class Budget implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id_budget")
    private Integer idbudget;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Annee")
    private int annee;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B_initial")
    private float binitial;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B_reestime")
    private float breestime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B_engage")
    private float bengage;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B_realise")
    private float brealise;
    @JoinColumn(name = "Projett_FK", referencedColumnName = "Ref_projet")
    @ManyToOne(optional = false)
    private Projet projettFK;

    public Budget() {
    }

    public Budget(Integer idbudget) {
        this.idbudget = idbudget;
    }

    public Budget(Integer idbudget, int annee, float binitial, float breestime, float bengage, float brealise) {
        this.idbudget = idbudget;
        this.annee = annee;
        this.binitial = binitial;
        this.breestime = breestime;
        this.bengage = bengage;
        this.brealise = brealise;
    }

    public Integer getIdbudget() {
        return idbudget;
    }

    public void setIdbudget(Integer idbudget) {
        this.idbudget = idbudget;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    public float getBinitial() {
        return binitial;
    }

    public void setBinitial(float binitial) {
        this.binitial = binitial;
    }

    public float getBreestime() {
        return breestime;
    }

    public void setBreestime(float breestime) {
        this.breestime = breestime;
    }

    public float getBengage() {
        return bengage;
    }

    public void setBengage(float bengage) {
        this.bengage = bengage;
    }

    public float getBrealise() {
        return brealise;
    }

    public void setBrealise(float brealise) {
        this.brealise = brealise;
    }

    public Projet getProjettFK() {
        return projettFK;
    }

    public void setProjettFK(Projet projettFK) {
        this.projettFK = projettFK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idbudget != null ? idbudget.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Budget)) {
            return false;
        }
        Budget other = (Budget) object;
        if ((this.idbudget == null && other.idbudget != null) || (this.idbudget != null && !this.idbudget.equals(other.idbudget))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uib.Budget[ idbudget=" + idbudget + " ]";
    }
    
}
