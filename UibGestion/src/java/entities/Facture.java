/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Sana
 */
@Entity
@Table(name = "facture")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Facture.findAll", query = "SELECT f FROM Facture f")
    , @NamedQuery(name = "Facture.findByIdfacture", query = "SELECT f FROM Facture f WHERE f.idfacture = :idfacture")
    , @NamedQuery(name = "Facture.findByDate", query = "SELECT f FROM Facture f WHERE f.date = :date")
    , @NamedQuery(name = "Facture.findByCodeclient", query = "SELECT f FROM Facture f WHERE f.codeclient = :codeclient")
    , @NamedQuery(name = "Facture.findByNumcodeclient", query = "SELECT f FROM Facture f WHERE f.numcodeclient = :numcodeclient")
    , @NamedQuery(name = "Facture.findByDatecmdclient", query = "SELECT f FROM Facture f WHERE f.datecmdclient = :datecmdclient")
    , @NamedQuery(name = "Facture.findByModedepaiement", query = "SELECT f FROM Facture f WHERE f.modedepaiement = :modedepaiement")
    , @NamedQuery(name = "Facture.findByRep", query = "SELECT f FROM Facture f WHERE f.rep = :rep")
    , @NamedQuery(name = "Facture.findByMontantHT", query = "SELECT f FROM Facture f WHERE f.montantHT = :montantHT")
    , @NamedQuery(name = "Facture.findByTva", query = "SELECT f FROM Facture f WHERE f.tva = :tva")
    , @NamedQuery(name = "Facture.findByBaseTVA", query = "SELECT f FROM Facture f WHERE f.baseTVA = :baseTVA")
    , @NamedQuery(name = "Facture.findByTaux", query = "SELECT f FROM Facture f WHERE f.taux = :taux")
    , @NamedQuery(name = "Facture.findByMontantTVA", query = "SELECT f FROM Facture f WHERE f.montantTVA = :montantTVA")
    , @NamedQuery(name = "Facture.findByTotalTVA", query = "SELECT f FROM Facture f WHERE f.totalTVA = :totalTVA")
    , @NamedQuery(name = "Facture.findByNetapayer", query = "SELECT f FROM Facture f WHERE f.netapayer = :netapayer")
    , @NamedQuery(name = "Facture.findByCommentaires", query = "SELECT f FROM Facture f WHERE f.commentaires = :commentaires")
    , @NamedQuery(name = "Facture.findByNumfacture", query = "SELECT f FROM Facture f WHERE f.numfacture = :numfacture")
    , @NamedQuery(name = "Facture.findByCharge", query = "SELECT f FROM Facture f WHERE f.charge = :charge")
    , @NamedQuery(name = "Facture.findByEmailsoc", query = "SELECT f FROM Facture f WHERE f.emailsoc = :emailsoc")
    , @NamedQuery(name = "Facture.findByTelsoc", query = "SELECT f FROM Facture f WHERE f.telsoc = :telsoc")
    , @NamedQuery(name = "Facture.findByClient", query = "SELECT f FROM Facture f WHERE f.client = :client")
    , @NamedQuery(name = "Facture.findByMatfiscale", query = "SELECT f FROM Facture f WHERE f.matfiscale = :matfiscale")
    , @NamedQuery(name = "Facture.findByContact", query = "SELECT f FROM Facture f WHERE f.contact = :contact")
    , @NamedQuery(name = "Facture.findByAdresse", query = "SELECT f FROM Facture f WHERE f.adresse = :adresse")
    , @NamedQuery(name = "Facture.findByEmailclient", query = "SELECT f FROM Facture f WHERE f.emailclient = :emailclient")
    , @NamedQuery(name = "Facture.findByCahierdecharge", query = "SELECT f FROM Facture f WHERE f.cahierdecharge = :cahierdecharge")
    , @NamedQuery(name = "Facture.findByPrixtotal", query = "SELECT f FROM Facture f WHERE f.prixtotal = :prixtotal")
    , @NamedQuery(name = "Facture.findByTotalHT", query = "SELECT f FROM Facture f WHERE f.totalHT = :totalHT")
    , @NamedQuery(name = "Facture.findByTimbrefiscale", query = "SELECT f FROM Facture f WHERE f.timbrefiscale = :timbrefiscale")
    , @NamedQuery(name = "Facture.findByTotalTTC", query = "SELECT f FROM Facture f WHERE f.totalTTC = :totalTTC")
    , @NamedQuery(name = "Facture.findByMontantfacture", query = "SELECT f FROM Facture f WHERE f.montantfacture = :montantfacture")
    , @NamedQuery(name = "Facture.findByDatedepaiement", query = "SELECT f FROM Facture f WHERE f.datedepaiement = :datedepaiement")
    , @NamedQuery(name = "Facture.findByObjet", query = "SELECT f FROM Facture f WHERE f.objet = :objet")
    , @NamedQuery(name = "Facture.findByPrestataire", query = "SELECT f FROM Facture f WHERE f.prestataire = :prestataire")
    , @NamedQuery(name = "Facture.findByDatedefacturation", query = "SELECT f FROM Facture f WHERE f.datedefacturation = :datedefacturation")
    , @NamedQuery(name = "Facture.findByResponsable", query = "SELECT f FROM Facture f WHERE f.responsable = :responsable")
    , @NamedQuery(name = "Facture.findByFournisseur", query = "SELECT f FROM Facture f WHERE f.fournisseur = :fournisseur")
    , @NamedQuery(name = "Facture.findByDomaine", query = "SELECT f FROM Facture f WHERE f.domaine = :domaine")
    , @NamedQuery(name = "Facture.findByType", query = "SELECT f FROM Facture f WHERE f.type = :type")
    , @NamedQuery(name = "Facture.findByFamille", query = "SELECT f FROM Facture f WHERE f.famille = :famille")})
public class Facture implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id_facture")
    private Integer idfacture;
    @Basic(optional = false)
    @Size(min = 1, max = 100)
    @Column(name = "Date")
    private String date;
    @Basic(optional = false)
    @Column(name = "Code_client")
    private int codeclient;
    @Basic(optional = false)
    @Column(name = "Num_code_client")
    private int numcodeclient;
    @Basic(optional = false)
    @Column(name = "Date_cmd_client")
    @Size(min = 1, max = 100)
    private String datecmdclient;
    @Basic(optional = false)
    @Size(min = 1, max = 100)
    @Column(name = "Mode_de_paiement")
    private String modedepaiement;
    @Basic(optional = false)
    @Size(min = 1, max = 100)
    @Column(name = "Rep")
    private String rep;
    @Basic(optional = false)
    @Column(name = "Montant_HT")
    private float montantHT;
    @Basic(optional = false)
    @Column(name = "TVA")
    private float tva;
    @Basic(optional = false)
    @Column(name = "Base_TVA")
    private float baseTVA;
    @Basic(optional = false)
    @Column(name = "Taux")
    private float taux;
    @Basic(optional = false)
    @Column(name = "Montant_TVA")
    private float montantTVA;
    @Basic(optional = false)
    @Column(name = "Total_TVA")
    private float totalTVA;
    @Basic(optional = false)
    @Column(name = "Net_a_payer")
    private float netapayer;
    @Basic(optional = false)
    @Size(min = 1, max = 1000)
    @Column(name = "Commentaires")
    private String commentaires;
    @Basic(optional = false)
    @Column(name = "Num_facture")
    private int numfacture;
    @Basic(optional = false)
    @Column(name = "Charge")
    @Size(min = 1, max = 100)
    private String charge;
    @Basic(optional = false)
    @Size(min = 1, max = 100)
    @Column(name = "Email_soc")
    private String emailsoc;
    @Basic(optional = false)
    @Column(name = "Tel_soc")
    private int telsoc;
    @Basic(optional = false)
    @Size(min = 1, max = 100)
    @Column(name = "Client")
    private String client;
    @Basic(optional = false)
    @Size(min = 1, max = 100)
    @Column(name = "Mat_fiscale")
    private String matfiscale;
    @Basic(optional = false)
    @Size(min = 1, max = 50)
    @Column(name = "Contact")
    private String contact;
    @Basic(optional = false)
    @Size(min = 1, max = 100)
    @Column(name = "Adresse")
    private String adresse;
    @Basic(optional = false)
    @Size(min = 1, max = 100)
    @Column(name = "Email_client")
    private String emailclient;
    @Basic(optional = false)
    @Size(min = 1, max = 1000)
    @Column(name = "Cahier_de_charge")
    private String cahierdecharge;
    @Basic(optional = false)
    @Column(name = "Prix_total")
    private float prixtotal;
    @Basic(optional = false)
    @Column(name = "Total_HT")
    private float totalHT;
    @Basic(optional = false)
    @Size(min = 1, max = 100)
    @Column(name = "Timbre_fiscale")
    private String timbrefiscale;
    @Basic(optional = false)
    @Column(name = "Total_TTC")
    private float totalTTC;
    @Basic(optional = false)
    @Column(name = "Montant_facture")
    private float montantfacture;
    @Basic(optional = false)
    @Column(name = "Date_de_paiement")
    @Size(min = 1, max = 100)
    private String datedepaiement;
    @Basic(optional = false)
    @Size(min = 1, max = 100)
    @Column(name = "Objet")
    private String objet;
    @Basic(optional = false)
    @Size(min = 1, max = 100)
    @Column(name = "Prestataire")
    private String prestataire;
    @Basic(optional = false)
    @Column(name = "Date_de_facturation")
    @Size(min = 1, max = 50)
    private String datedefacturation;
    @Basic(optional = false)
    @Size(min = 1, max = 50)
    @Column(name = "Responsable")
    private String responsable;
    @Basic(optional = false)
    @Size(min = 1, max = 50)
    @Column(name = "Fournisseur")
    private String fournisseur;
    @Basic(optional = false)
    @Size(min = 1, max = 50)
    @Column(name = "Domaine")
    private String domaine;
    @Basic(optional = false)
    @Size(min = 1, max = 10)
    @Column(name = "Type")
    private String type;
    @Basic(optional = false)
    @Size(min = 1, max = 10)
    @Column(name = "Famille")
    private String famille;
    @JoinColumn(name = "DCP_FK", referencedColumnName = "RefBc")
    @ManyToOne(optional = false)
    private BcDsiProjet dcpFk;
    @JoinColumn(name = "BC_FK", referencedColumnName = "Ref_BC")
    @ManyToOne(optional = false)
    private BonCommande bcFk;
    @JoinColumn(name = "Contrat_FK", referencedColumnName = "Reference")
    @ManyToOne(optional = false)
    private Contrat contratFK;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "factureFk")
    private List<DetailFacture> detailFactureList;

    public Facture() {
    }

    public Facture(Integer idfacture) {
        this.idfacture = idfacture;
    }

    public Facture(Integer idfacture, String date, int codeclient, int numcodeclient, String datecmdclient, String modedepaiement, String rep, float montantHT, float tva, float baseTVA, float taux, float montantTVA, float totalTVA, float netapayer, String commentaires, int numfacture, String charge, String emailsoc, int telsoc, String client, String matfiscale, String contact, String adresse, String emailclient, String cahierdecharge, float prixtotal, float totalHT, String timbrefiscale, float totalTTC, float montantfacture, String datedepaiement, String objet, String prestataire, String datedefacturation, String responsable, String fournisseur, String domaine, String type, String famille) {
        this.idfacture = idfacture;
        this.date = date;
        this.codeclient = codeclient;
        this.numcodeclient = numcodeclient;
        this.datecmdclient = datecmdclient;
        this.modedepaiement = modedepaiement;
        this.rep = rep;
        this.montantHT = montantHT;
        this.tva = tva;
        this.baseTVA = baseTVA;
        this.taux = taux;
        this.montantTVA = montantTVA;
        this.totalTVA = totalTVA;
        this.netapayer = netapayer;
        this.commentaires = commentaires;
        this.numfacture = numfacture;
        this.charge = charge;
        this.emailsoc = emailsoc;
        this.telsoc = telsoc;
        this.client = client;
        this.matfiscale = matfiscale;
        this.contact = contact;
        this.adresse = adresse;
        this.emailclient = emailclient;
        this.cahierdecharge = cahierdecharge;
        this.prixtotal = prixtotal;
        this.totalHT = totalHT;
        this.timbrefiscale = timbrefiscale;
        this.totalTTC = totalTTC;
        this.montantfacture = montantfacture;
        this.datedepaiement = datedepaiement;
        this.objet = objet;
        this.prestataire = prestataire;
        this.datedefacturation = datedefacturation;
        this.responsable = responsable;
        this.fournisseur = fournisseur;
        this.domaine = domaine;
        this.type = type;
        this.famille = famille;
    }

    public Integer getIdfacture() {
        return idfacture;
    }

    public void setIdfacture(Integer idfacture) {
        this.idfacture = idfacture;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getCodeclient() {
        return codeclient;
    }

    public void setCodeclient(int codeclient) {
        this.codeclient = codeclient;
    }

    public int getNumcodeclient() {
        return numcodeclient;
    }

    public void setNumcodeclient(int numcodeclient) {
        this.numcodeclient = numcodeclient;
    }

    public String getDatecmdclient() {
        return datecmdclient;
    }

    public void setDatecmdclient(String datecmdclient) {
        this.datecmdclient = datecmdclient;
    }

    public String getModedepaiement() {
        return modedepaiement;
    }

    public void setModedepaiement(String modedepaiement) {
        this.modedepaiement = modedepaiement;
    }

    public String getRep() {
        return rep;
    }

    public void setRep(String rep) {
        this.rep = rep;
    }

    public float getMontantHT() {
        return montantHT;
    }

    public void setMontantHT(float montantHT) {
        this.montantHT = montantHT;
    }

    public float getTva() {
        return tva;
    }

    public void setTva(float tva) {
        this.tva = tva;
    }

    public float getBaseTVA() {
        return baseTVA;
    }

    public void setBaseTVA(float baseTVA) {
        this.baseTVA = baseTVA;
    }

    public float getTaux() {
        return taux;
    }

    public void setTaux(float taux) {
        this.taux = taux;
    }

    public float getMontantTVA() {
        return montantTVA;
    }

    public void setMontantTVA(float montantTVA) {
        this.montantTVA = montantTVA;
    }

    public float getTotalTVA() {
        return totalTVA;
    }

    public void setTotalTVA(float totalTVA) {
        this.totalTVA = totalTVA;
    }

    public float getNetapayer() {
        return netapayer;
    }

    public void setNetapayer(float netapayer) {
        this.netapayer = netapayer;
    }

    public String getCommentaires() {
        return commentaires;
    }

    public void setCommentaires(String commentaires) {
        this.commentaires = commentaires;
    }

    public int getNumfacture() {
        return numfacture;
    }

    public void setNumfacture(int numfacture) {
        this.numfacture = numfacture;
    }

    public String getCharge() {
        return charge;
    }

    public void setCharge(String charge) {
        this.charge = charge;
    }

    public String getEmailsoc() {
        return emailsoc;
    }

    public void setEmailsoc(String emailsoc) {
        this.emailsoc = emailsoc;
    }

    public int getTelsoc() {
        return telsoc;
    }

    public void setTelsoc(int telsoc) {
        this.telsoc = telsoc;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getMatfiscale() {
        return matfiscale;
    }

    public void setMatfiscale(String matfiscale) {
        this.matfiscale = matfiscale;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getEmailclient() {
        return emailclient;
    }

    public void setEmailclient(String emailclient) {
        this.emailclient = emailclient;
    }

    public String getCahierdecharge() {
        return cahierdecharge;
    }

    public void setCahierdecharge(String cahierdecharge) {
        this.cahierdecharge = cahierdecharge;
    }

    public float getPrixtotal() {
        return prixtotal;
    }

    public void setPrixtotal(float prixtotal) {
        this.prixtotal = prixtotal;
    }

    public float getTotalHT() {
        return totalHT;
    }

    public void setTotalHT(float totalHT) {
        this.totalHT = totalHT;
    }

    public String getTimbrefiscale() {
        return timbrefiscale;
    }

    public void setTimbrefiscale(String timbrefiscale) {
        this.timbrefiscale = timbrefiscale;
    }

    public float getTotalTTC() {
        return totalTTC;
    }

    public void setTotalTTC(float totalTTC) {
        this.totalTTC = totalTTC;
    }

    public float getMontantfacture() {
        return montantfacture;
    }

    public void setMontantfacture(float montantfacture) {
        this.montantfacture = montantfacture;
    }

    public String getDatedepaiement() {
        return datedepaiement;
    }

    public void setDatedepaiement(String datedepaiement) {
        this.datedepaiement = datedepaiement;
    }

    public String getObjet() {
        return objet;
    }

    public void setObjet(String objet) {
        this.objet = objet;
    }

    public String getPrestataire() {
        return prestataire;
    }

    public void setPrestataire(String prestataire) {
        this.prestataire = prestataire;
    }

    public String getDatedefacturation() {
        return datedefacturation;
    }

    public void setDatedefacturation(String datedefacturation) {
        this.datedefacturation = datedefacturation;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(String fournisseur) {
        this.fournisseur = fournisseur;
    }

    public String getDomaine() {
        return domaine;
    }

    public void setDomaine(String domaine) {
        this.domaine = domaine;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFamille() {
        return famille;
    }

    public void setFamille(String famille) {
        this.famille = famille;
    }

    public BcDsiProjet getDcpFk() {
        return dcpFk;
    }

    public void setDcpFk(BcDsiProjet dcpFk) {
        this.dcpFk = dcpFk;
    }

    public BonCommande getBcFk() {
        return bcFk;
    }

    public void setBcFk(BonCommande bcFk) {
        this.bcFk = bcFk;
    }

    public Contrat getContratFK() {
        return contratFK;
    }

    public void setContratFK(Contrat contratFK) {
        this.contratFK = contratFK;
    }

    @XmlTransient
    public List<DetailFacture> getDetailFactureList() {
        return detailFactureList;
    }

    public void setDetailFactureList(List<DetailFacture> detailFactureList) {
        this.detailFactureList = detailFactureList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idfacture != null ? idfacture.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Facture)) {
            return false;
        }
        Facture other = (Facture) object;
        if ((this.idfacture == null && other.idfacture != null) || (this.idfacture != null && !this.idfacture.equals(other.idfacture))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uib.Facture[ idfacture=" + idfacture + " ]";
    }

}
