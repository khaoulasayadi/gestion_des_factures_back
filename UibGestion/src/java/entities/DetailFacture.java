/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Sana
 */
@Entity
@Table(name = "detail_facture")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailFacture.findAll", query = "SELECT d FROM DetailFacture d")
    , @NamedQuery(name = "DetailFacture.findByCodearticle", query = "SELECT d FROM DetailFacture d WHERE d.codearticle = :codearticle")
    , @NamedQuery(name = "DetailFacture.findByDesignation", query = "SELECT d FROM DetailFacture d WHERE d.designation = :designation")
    , @NamedQuery(name = "DetailFacture.findByQuantite", query = "SELECT d FROM DetailFacture d WHERE d.quantite = :quantite")
    , @NamedQuery(name = "DetailFacture.findByPUHt", query = "SELECT d FROM DetailFacture d WHERE d.puhtt = :puhtt")
    , @NamedQuery(name = "DetailFacture.findByPUHi", query = "SELECT d FROM DetailFacture d WHERE d.puhii = :puhii")
    , @NamedQuery(name = "DetailFacture.findByIddet", query = "SELECT d FROM DetailFacture d WHERE d.iddet = :iddet")})
public class DetailFacture implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Code_article")
    private int codearticle;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Designation")
    private String designation;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Quantite")
    private int quantite;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "P_U_HT")
    private String puhtt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "P_U_HI")
    private String puhii;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Id_det")
    private Integer iddet;
    @JoinColumn(name = "Facture_Fk", referencedColumnName = "Id_facture")
    @ManyToOne(optional = false)
    private Facture factureFk;

    public DetailFacture() {
    }

    public DetailFacture(Integer iddet) {
        this.iddet = iddet;
    }

    public DetailFacture(Integer iddet, int codearticle, String designation, int quantite, String puht, String puhi) {
        this.iddet = iddet;
        this.codearticle = codearticle;
        this.designation = designation;
        this.quantite = quantite;
        this.puhtt = puht;
        this.puhii = puhi;
    }

    public int getCodearticle() {
        return codearticle;
    }

    public void setCodearticle(int codearticle) {
        this.codearticle = codearticle;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public String getPUHt() {
        return puhtt;
    }

    public void setPUHt(String puht) {
        this.puhtt = puht;
    }

    public String getPUHi() {
        return puhii;
    }

    public void setPUHi(String puhi) {
        this.puhii = puhi;
    }

    public Integer getIddet() {
        return iddet;
    }

    public void setIddet(Integer iddet) {
        this.iddet = iddet;
    }

    public Facture getFactureFk() {
        return factureFk;
    }

    public void setFactureFk(Facture factureFk) {
        this.factureFk = factureFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iddet != null ? iddet.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailFacture)) {
            return false;
        }
        DetailFacture other = (DetailFacture) object;
        if ((this.iddet == null && other.iddet != null) || (this.iddet != null && !this.iddet.equals(other.iddet))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uib.DetailFacture[ iddet=" + iddet + " ]";
    }

}
