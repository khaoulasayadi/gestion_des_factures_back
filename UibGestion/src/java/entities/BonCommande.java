/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Sana
 */
@Entity
@Table(name = "bon_commande")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BonCommande.findAll", query = "SELECT b FROM BonCommande b")
    , @NamedQuery(name = "BonCommande.findByRefBC", query = "SELECT b FROM BonCommande b WHERE b.refBC = :refBC")
    , @NamedQuery(name = "BonCommande.findByDatesignature", query = "SELECT b FROM BonCommande b WHERE b.datesignature = :datesignature")
    , @NamedQuery(name = "BonCommande.findByPrestataire", query = "SELECT b FROM BonCommande b WHERE b.prestataire = :prestataire")
    , @NamedQuery(name = "BonCommande.findByResponsable", query = "SELECT b FROM BonCommande b WHERE b.responsable = :responsable")
    , @NamedQuery(name = "BonCommande.findByCommentaires", query = "SELECT b FROM BonCommande b WHERE b.commentaires = :commentaires")
    , @NamedQuery(name = "BonCommande.findByDateintervention", query = "SELECT b FROM BonCommande b WHERE b.dateintervention = :dateintervention")
    , @NamedQuery(name = "BonCommande.findByCoutTTC", query = "SELECT b FROM BonCommande b WHERE b.coutTTC = :coutTTC")
    , @NamedQuery(name = "BonCommande.findByCoutHT", query = "SELECT b FROM BonCommande b WHERE b.coutHT = :coutHT")
    , @NamedQuery(name = "BonCommande.findByPiecesjointes", query = "SELECT b FROM BonCommande b WHERE b.piecesjointes = :piecesjointes")})
public class BonCommande implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @NotNull
    @Basic(optional = false)
    @Column(name = "Ref_BC")
    private Integer refBC;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Date_signature")
    @Size(min = 1, max = 100)
    private String datesignature;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Prestataire")
    private String prestataire;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Responsable")
    private String responsable;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "Commentaires")
    private String commentaires;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Date_intervention")
    @Size(min = 1, max = 100)
    private String dateintervention;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Cout_TTC")
    private float coutTTC;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Cout_HT")
    private float coutHT;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Pieces_jointes")
    private String piecesjointes;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bcmFk")
    private List<Materiel> matérielList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bcFk")
    private List<Facture> factureList;

    public BonCommande() {
    }

    public BonCommande(Integer refBC) {
        this.refBC = refBC;
    }

    public BonCommande(Integer refBC, String datesignature, String prestataire, String responsable, String commentaires, String dateintervention, float coutTTC, float coutHT, String piecesjointes) {
        this.refBC = refBC;
        this.datesignature = datesignature;
        this.prestataire = prestataire;
        this.responsable = responsable;
        this.commentaires = commentaires;
        this.dateintervention = dateintervention;
        this.coutTTC = coutTTC;
        this.coutHT = coutHT;
        this.piecesjointes = piecesjointes;
    }

    public Integer getRefBC() {
        return refBC;
    }

    public void setRefBC(Integer refBC) {
        this.refBC = refBC;
    }

    public String getDatesignature() {
        return datesignature;
    }

    public void setDatesignature(String datesignature) {
        this.datesignature = datesignature;
    }

    public String getPrestataire() {
        return prestataire;
    }

    public void setPrestataire(String prestataire) {
        this.prestataire = prestataire;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getCommentaires() {
        return commentaires;
    }

    public void setCommentaires(String commentaires) {
        this.commentaires = commentaires;
    }

    public String getDateintervention() {
        return dateintervention;
    }

    public void setDateintervention(String dateintervention) {
        this.dateintervention = dateintervention;
    }

    public float getCoutTTC() {
        return coutTTC;
    }

    public void setCoutTTC(float coutTTC) {
        this.coutTTC = coutTTC;
    }

    public float getCoutHT() {
        return coutHT;
    }

    public void setCoutHT(float coutHT) {
        this.coutHT = coutHT;
    }

    public String getPiecesjointes() {
        return piecesjointes;
    }

    public void setPiecesjointes(String piecesjointes) {
        this.piecesjointes = piecesjointes;
    }

    @XmlTransient
    public List<Materiel> getMatérielList() {
        return matérielList;
    }

    public void setMatérielList(List<Materiel> matérielList) {
        this.matérielList = matérielList;
    }

    @XmlTransient
    public List<Facture> getFactureList() {
        return factureList;
    }

    public void setFactureList(List<Facture> factureList) {
        this.factureList = factureList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refBC != null ? refBC.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BonCommande)) {
            return false;
        }
        BonCommande other = (BonCommande) object;
        if ((this.refBC == null && other.refBC != null) || (this.refBC != null && !this.refBC.equals(other.refBC))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uib.BonCommande[ refBC=" + refBC + " ]";
    }
    
}
