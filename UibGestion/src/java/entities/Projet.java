/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Sana
 */
@Entity
@Table(name = "projet")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Projet.findAll", query = "SELECT p FROM Projet p")
    , @NamedQuery(name = "Projet.findByRefprojet", query = "SELECT p FROM Projet p WHERE p.refprojet = :refprojet")
    , @NamedQuery(name = "Projet.findByIntitule", query = "SELECT p FROM Projet p WHERE p.intitule = :intitule")
    , @NamedQuery(name = "Projet.findByDescription", query = "SELECT p FROM Projet p WHERE p.description = :description")
    , @NamedQuery(name = "Projet.findByResponsable", query = "SELECT p FROM Projet p WHERE p.responsable = :responsable")
    , @NamedQuery(name = "Projet.findByDatedebut", query = "SELECT p FROM Projet p WHERE p.datedebut = :datedebut")
    , @NamedQuery(name = "Projet.findByDatefin", query = "SELECT p FROM Projet p WHERE p.datefin = :datefin")
    , @NamedQuery(name = "Projet.findByCommentaires", query = "SELECT p FROM Projet p WHERE p.commentaires = :commentaires")
    , @NamedQuery(name = "Projet.findByPiecesjointes", query = "SELECT p FROM Projet p WHERE p.piecesjointes = :piecesjointes")})
public class Projet implements Serializable {

    private static final long serialVersionUID = 1L;
   
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Ref_projet")
    private Integer refprojet;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Intitule")
    private String intitule;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "Description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Responsable")
    private String responsable;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Date_debut")
    @Size(min = 1, max = 100)
    private String datedebut;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Date_fin")
    @Size(min = 1, max = 100)
    private String datefin;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "Commentaires")
    private String commentaires;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Pieces_jointes")
    private String piecesjointes;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projetFK")
    private List<BcDsiProjet> bcDsiProjetList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projettFK")
    private List<Budget> budgetList;

    public Projet() {
    }

    public Projet(Integer refprojet) {
        this.refprojet = refprojet;
    }

    public Projet(Integer refprojet, String intitule, String description, String responsable, String datedebut, String datefin, String commentaires, String piecesjointes) {
        this.refprojet = refprojet;
        this.intitule = intitule;
        this.description = description;
        this.responsable = responsable;
        this.datedebut = datedebut;
        this.datefin = datefin;
        this.commentaires = commentaires;
        this.piecesjointes = piecesjointes;
    }

    public Integer getRefprojet() {
        return refprojet;
    }

    public void setRefprojet(Integer refprojet) {
        this.refprojet = refprojet;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getDatedebut() {
        return datedebut;
    }

    public void setDatedebut(String datedebut) {
        this.datedebut = datedebut;
    }

    public String getDatefin() {
        return datefin;
    }

    public void setDatefin(String datefin) {
        this.datefin = datefin;
    }

    public String getCommentaires() {
        return commentaires;
    }

    public void setCommentaires(String commentaires) {
        this.commentaires = commentaires;
    }

    public String getPiecesjointes() {
        return piecesjointes;
    }

    public void setPiecesjointes(String piecesjointes) {
        this.piecesjointes = piecesjointes;
    }

    @XmlTransient
    public List<BcDsiProjet> getBcDsiProjetList() {
        return bcDsiProjetList;
    }

    public void setBcDsiProjetList(List<BcDsiProjet> bcDsiProjetList) {
        this.bcDsiProjetList = bcDsiProjetList;
    }

    @XmlTransient
    public List<Budget> getBudgetList() {
        return budgetList;
    }

    public void setBudgetList(List<Budget> budgetList) {
        this.budgetList = budgetList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refprojet != null ? refprojet.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Projet)) {
            return false;
        }
        Projet other = (Projet) object;
        if ((this.refprojet == null && other.refprojet != null) || (this.refprojet != null && !this.refprojet.equals(other.refprojet))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uib.Projet[ refprojet=" + refprojet + " ]";
    }
    
}
