/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Sana
 */
@Entity
@Table(name = "intervention")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Intervention.findAll", query = "SELECT i FROM Intervention i")
    , @NamedQuery(name = "Intervention.findByDateintv", query = "SELECT i FROM Intervention i WHERE i.dateintv = :dateintv")
    , @NamedQuery(name = "Intervention.findByNbredepersonnes", query = "SELECT i FROM Intervention i WHERE i.nbredepersonnes = :nbredepersonnes")
    , @NamedQuery(name = "Intervention.findByDuree", query = "SELECT i FROM Intervention i WHERE i.duree = :duree")
    , @NamedQuery(name = "Intervention.findByCoutannuel", query = "SELECT i FROM Intervention i WHERE i.coutannuel = :coutannuel")
    , @NamedQuery(name = "Intervention.findByDescription", query = "SELECT i FROM Intervention i WHERE i.description = :description")
    , @NamedQuery(name = "Intervention.findByPiecejointe", query = "SELECT i FROM Intervention i WHERE i.piecejointe = :piecejointe")
    , @NamedQuery(name = "Intervention.findByIdInterv", query = "SELECT i FROM Intervention i WHERE i.idInterv = :idInterv")})
public class Intervention implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "Date_intv")
    private String dateintv;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Nbre_de_personnes")
    private int nbredepersonnes;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Duree")
    private int duree;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Cout_annuel")
    private float coutannuel;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "Description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "Piece_jointe")
    private String piecejointe;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_interv")
    private Integer idInterv;
    @JoinColumn(name = "Contrat_FK", referencedColumnName = "Reference")
    @ManyToOne(optional = false)
    private Contrat contratFK;

    public Intervention() {
    }

    public Intervention(Integer idInterv) {
        this.idInterv = idInterv;
    }

    public Intervention(Integer idInterv, String dateintv, int nbredepersonnes, int duree, float coutannuel, String description, String piecejointe) {
        this.idInterv = idInterv;
        this.dateintv = dateintv;
        this.nbredepersonnes = nbredepersonnes;
        this.duree = duree;
        this.coutannuel = coutannuel;
        this.description = description;
        this.piecejointe = piecejointe;
    }

    public String getDateintv() {
        return dateintv;
    }

    public void setDateintv(String dateintv) {
        this.dateintv = dateintv;
    }

    public int getNbredepersonnes() {
        return nbredepersonnes;
    }

    public void setNbredepersonnes(int nbredepersonnes) {
        this.nbredepersonnes = nbredepersonnes;
    }

    public int getDuree() {
        return duree;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }

    public float getCoutannuel() {
        return coutannuel;
    }

    public void setCoutannuel(float coutannuel) {
        this.coutannuel = coutannuel;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPiecejointe() {
        return piecejointe;
    }

    public void setPiecejointe(String piecejointe) {
        this.piecejointe = piecejointe;
    }

    public Integer getIdInterv() {
        return idInterv;
    }

    public void setIdInterv(Integer idInterv) {
        this.idInterv = idInterv;
    }

    public Contrat getContratFK() {
        return contratFK;
    }

    public void setContratFK(Contrat contratFK) {
        this.contratFK = contratFK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idInterv != null ? idInterv.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Intervention)) {
            return false;
        }
        Intervention other = (Intervention) object;
        if ((this.idInterv == null && other.idInterv != null) || (this.idInterv != null && !this.idInterv.equals(other.idInterv))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uib.Intervention[ idInterv=" + idInterv + " ]";
    }
    
}
