/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Sana
 */
@Entity
@Table(name = "detail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Detail.findAll", query = "SELECT d FROM Detail d")
    , @NamedQuery(name = "Detail.findByIddetail", query = "SELECT d FROM Detail d WHERE d.iddetail = :iddetail")
    , @NamedQuery(name = "Detail.findByType", query = "SELECT d FROM Detail d WHERE d.type = :type")
    , @NamedQuery(name = "Detail.findByElement", query = "SELECT d FROM Detail d WHERE d.element = :element")
    , @NamedQuery(name = "Detail.findByCodearticle", query = "SELECT d FROM Detail d WHERE d.codearticle = :codearticle")
    , @NamedQuery(name = "Detail.findByQuantite", query = "SELECT d FROM Detail d WHERE d.quantite = :quantite")
    , @NamedQuery(name = "Detail.findByPUIBrut", query = "SELECT d FROM Detail d WHERE d.pUIBrut = :pUIBrut")
    , @NamedQuery(name = "Detail.findByRemise", query = "SELECT d FROM Detail d WHERE d.remise = :remise")
    , @NamedQuery(name = "Detail.findByPUNet", query = "SELECT d FROM Detail d WHERE d.pUNet = :pUNet")
    , @NamedQuery(name = "Detail.findByTotalHT", query = "SELECT d FROM Detail d WHERE d.totalHT = :totalHT")
    , @NamedQuery(name = "Detail.findByFrais", query = "SELECT d FROM Detail d WHERE d.frais = :frais")
    , @NamedQuery(name = "Detail.findByTaxes", query = "SELECT d FROM Detail d WHERE d.taxes = :taxes")})
public class Detail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id_detail")
    private Integer iddetail;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "Type")
    private String type;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Element")
    private String element;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Code_article")
    private int codearticle;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Quantite")
    private int quantite;
    @Basic(optional = false)
    @NotNull
    @Column(name = "P_UI_Brut")
    private float pUIBrut;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Remise")
    private float remise;
    @Basic(optional = false)
    @NotNull
    @Column(name = "P_U_Net")
    private float pUNet;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Total_HT")
    private float totalHT;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Frais")
    private float frais;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Taxes")
    private float taxes;
    @JoinColumn(name = "dsii_FK", referencedColumnName = "RefBc")
    @ManyToOne(optional = false)
    private BcDsiProjet dsiiFK;

    public Detail() {
    }

    public Detail(Integer iddetail) {
        this.iddetail = iddetail;
    }

    public Detail(Integer iddetail, String type, String element, int codearticle, int quantite, float pUIBrut, float remise, float pUNet, float totalHT, float frais, float taxes) {
        this.iddetail = iddetail;
        this.type = type;
        this.element = element;
        this.codearticle = codearticle;
        this.quantite = quantite;
        this.pUIBrut = pUIBrut;
        this.remise = remise;
        this.pUNet = pUNet;
        this.totalHT = totalHT;
        this.frais = frais;
        this.taxes = taxes;
    }

    public Integer getIddetail() {
        return iddetail;
    }

    public void setIddetail(Integer iddetail) {
        this.iddetail = iddetail;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public int getCodearticle() {
        return codearticle;
    }

    public void setCodearticle(int codearticle) {
        this.codearticle = codearticle;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public float getPUIBrut() {
        return pUIBrut;
    }

    public void setPUIBrut(float pUIBrut) {
        this.pUIBrut = pUIBrut;
    }

    public float getRemise() {
        return remise;
    }

    public void setRemise(float remise) {
        this.remise = remise;
    }

    public float getPUNet() {
        return pUNet;
    }

    public void setPUNet(float pUNet) {
        this.pUNet = pUNet;
    }

    public float getTotalHT() {
        return totalHT;
    }

    public void setTotalHT(float totalHT) {
        this.totalHT = totalHT;
    }

    public float getFrais() {
        return frais;
    }

    public void setFrais(float frais) {
        this.frais = frais;
    }

    public float getTaxes() {
        return taxes;
    }

    public void setTaxes(float taxes) {
        this.taxes = taxes;
    }

    public BcDsiProjet getDsiiFK() {
        return dsiiFK;
    }

    public void setDsiiFK(BcDsiProjet dsiiFK) {
        this.dsiiFK = dsiiFK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iddetail != null ? iddetail.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Detail)) {
            return false;
        }
        Detail other = (Detail) object;
        if ((this.iddetail == null && other.iddetail != null) || (this.iddetail != null && !this.iddetail.equals(other.iddetail))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uib.Detail[ iddetail=" + iddetail + " ]";
    }
    
}
