/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Sana
 */
@Entity
@Table(name = "contrat")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contrat.findAll", query = "SELECT c FROM Contrat c")
    , @NamedQuery(name = "Contrat.findByReference", query = "SELECT c FROM Contrat c WHERE c.reference = :reference")
    , @NamedQuery(name = "Contrat.findByType", query = "SELECT c FROM Contrat c WHERE c.type = :type")
    , @NamedQuery(name = "Contrat.findByStatut", query = "SELECT c FROM Contrat c WHERE c.statut = :statut")
    , @NamedQuery(name = "Contrat.findByLibelle", query = "SELECT c FROM Contrat c WHERE c.libelle = :libelle")
    , @NamedQuery(name = "Contrat.findByFournisseur", query = "SELECT c FROM Contrat c WHERE c.fournisseur = :fournisseur")
    , @NamedQuery(name = "Contrat.findByFrs", query = "SELECT c FROM Contrat c WHERE c.frs = :frs")
    , @NamedQuery(name = "Contrat.findByDatesignature", query = "SELECT c FROM Contrat c WHERE c.datesignature = :datesignature")
    , @NamedQuery(name = "Contrat.findByDatedernierefact", query = "SELECT c FROM Contrat c WHERE c.datedernierefact = :datedernierefact")
    , @NamedQuery(name = "Contrat.findByFreqpaiement", query = "SELECT c FROM Contrat c WHERE c.freqpaiement = :freqpaiement")
    , @NamedQuery(name = "Contrat.findByRtbCtb", query = "SELECT c FROM Contrat c WHERE c.rtbCtb = :rtbCtb")
    , @NamedQuery(name = "Contrat.findByMTcontrat", query = "SELECT c FROM Contrat c WHERE c.mTcontrat = :mTcontrat")
    , @NamedQuery(name = "Contrat.findByAugmentation", query = "SELECT c FROM Contrat c WHERE c.augmentation = :augmentation")
    , @NamedQuery(name = "Contrat.findByPeriodedernierefact", query = "SELECT c FROM Contrat c WHERE c.periodedernierefact = :periodedernierefact")
    , @NamedQuery(name = "Contrat.findByMTHTdernierefact", query = "SELECT c FROM Contrat c WHERE c.mTHTdernierefact = :mTHTdernierefact")
    , @NamedQuery(name = "Contrat.findByMontantendevises", query = "SELECT c FROM Contrat c WHERE c.montantendevises = :montantendevises")
    , @NamedQuery(name = "Contrat.findByNbreinterv", query = "SELECT c FROM Contrat c WHERE c.nbreinterv = :nbreinterv")
    , @NamedQuery(name = "Contrat.findByRUNCHANGEbank", query = "SELECT c FROM Contrat c WHERE c.RUNCHANGEbank = :RUNCHANGEbank")
    , @NamedQuery(name = "Contrat.findByCommentaires", query = "SELECT c FROM Contrat c WHERE c.commentaires = :commentaires")
    , @NamedQuery(name = "Contrat.findByCommentairesPSO", query = "SELECT c FROM Contrat c WHERE c.commentairesPSO = :commentairesPSO")
    , @NamedQuery(name = "Contrat.findByPiecesjointes", query = "SELECT c FROM Contrat c WHERE c.piecesjointes = :piecesjointes")})
public class Contrat implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Reference")
    private Integer reference;
    @Basic(optional = false)
    @Size(min = 1, max = 3)
    @Column(name = "Type")
    private String type;
    @Basic(optional = false)
    @Size(min = 1, max = 100)
    @Column(name = "Statut")
    private String statut;
    @Basic(optional = false)
    @Size(min = 1, max = 50)
    @Column(name = "Libelle")
    private String libelle;
    @Basic(optional = false)
    @Size(min = 1, max = 50)
    @Column(name = "Fournisseur")
    private String fournisseur;
    @Basic(optional = false)
    @Size(min = 1, max = 50)
    @Column(name = "FRS")
    private String frs;
    @Basic(optional = false)
    @Size(min = 1, max = 50)
    @Column(name = "Date_signature")
    private String datesignature;
    @Basic(optional = false)
    @Column(name = "Date_dernierefact")
    @Size(min = 1, max = 50)
    private String datedernierefact;
    @Basic(optional = false)
    @Column(name = "Freq_paiement")
    private int freqpaiement;
    @Basic(optional = false)
    @Size(min = 1, max = 50)
    @Column(name = "RTB_CTB")
    private String rtbCtb;
    @Basic(optional = false)
    @Column(name = "MT_contrat")
    private float mTcontrat;
    @Basic(optional = false)
    @Size(min = 1, max = 50)
    @Column(name = "Augmentation")
    private String augmentation;
    @Basic(optional = false)
    @Size(min = 1, max = 50)
    @Column(name = "Periode_dernierefact")
    private String periodedernierefact;
    @Basic(optional = false)
    @Column(name = "MT_HT_dernierefact")
    private float mTHTdernierefact;
    @Basic(optional = false)
    @Column(name = "Montant_endevises")
    private float montantendevises;
    @Basic(optional = false)
    @Column(name = "Nbre_interv")
    private int nbreinterv;
    @Basic(optional = false)
    @Size(min = 1, max = 50)
    @Column(name = "RUN_CHANGEbank")
    private String RUNCHANGEbank;
    @Basic(optional = false)
    @Size(min = 1, max = 100)
    @Column(name = "Commentaires")
    private String commentaires;
    @Basic(optional = false)
    @Size(min = 1, max = 100)
    @Column(name = "CommentairesPSO")
    private String commentairesPSO;
    @Basic(optional = false)
    @Size(min = 1, max = 50)
    @Column(name = "Pieces_jointes")
    private String piecesjointes;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contratFK")
    private List<Facture> factureList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contratFK")
    private List<Intervention> interventionList;

    public Contrat() {
    }

    public Contrat(Integer reference) {
        this.reference = reference;
    }

    public Contrat(Integer reference, String type, String statut, String libelle, String fournisseur, String frs, String datesignature, String datedernierefact, int freqpaiement, String rtbCtb, float mTcontrat, String augmentation, String periodedernierefact, float mTHTdernierefact, float montantendevises, int nbreinterv, String RUNCHANGEbank, String commentaires, String commentairesPSO, String piecesjointes) {
        this.reference = reference;
        this.type = type;
        this.statut = statut;
        this.libelle = libelle;
        this.fournisseur = fournisseur;
        this.frs = frs;
        this.datesignature = datesignature;
        this.datedernierefact = datedernierefact;
        this.freqpaiement = freqpaiement;
        this.rtbCtb = rtbCtb;
        this.mTcontrat = mTcontrat;
        this.augmentation = augmentation;
        this.periodedernierefact = periodedernierefact;
        this.mTHTdernierefact = mTHTdernierefact;
        this.montantendevises = montantendevises;
        this.nbreinterv = nbreinterv;
        this.RUNCHANGEbank = RUNCHANGEbank;
        this.commentaires = commentaires;
        this.commentairesPSO = commentairesPSO;
        this.piecesjointes = piecesjointes;
    }

    public Integer getReference() {
        return reference;
    }

    public void setReference(Integer reference) {
        this.reference = reference;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(String fournisseur) {
        this.fournisseur = fournisseur;
    }

    public String getFrs() {
        return frs;
    }

    public void setFrs(String frs) {
        this.frs = frs;
    }

    public String getDatesignature() {
        return datesignature;
    }

    public void setDatesignature(String datesignature) {
        this.datesignature = datesignature;
    }

    public String getDatedernierefact() {
        return datedernierefact;
    }

    public void setDatedernierefact(String datedernierefact) {
        this.datedernierefact = datedernierefact;
    }

    public int getFreqpaiement() {
        return freqpaiement;
    }

    public void setFreqpaiement(int freqpaiement) {
        this.freqpaiement = freqpaiement;
    }

    public String getRtbCtb() {
        return rtbCtb;
    }

    public void setRtbCtb(String rtbCtb) {
        this.rtbCtb = rtbCtb;
    }

    public float getMTcontrat() {
        return mTcontrat;
    }

    public void setMTcontrat(float mTcontrat) {
        this.mTcontrat = mTcontrat;
    }

    public String getAugmentation() {
        return augmentation;
    }

    public void setAugmentation(String augmentation) {
        this.augmentation = augmentation;
    }

    public String getPeriodedernierefact() {
        return periodedernierefact;
    }

    public void setPeriodedernierefact(String periodedernierefact) {
        this.periodedernierefact = periodedernierefact;
    }

    public float getMTHTdernierefact() {
        return mTHTdernierefact;
    }

    public void setMTHTdernierefact(float mTHTdernierefact) {
        this.mTHTdernierefact = mTHTdernierefact;
    }

    public float getMontantendevises() {
        return montantendevises;
    }

    public void setMontantendevises(float montantendevises) {
        this.montantendevises = montantendevises;
    }

    public int getNbreinterv() {
        return nbreinterv;
    }

    public void setNbreinterv(int nbreinterv) {
        this.nbreinterv = nbreinterv;
    }

    public String getRUNCHANGEbank() {
        return RUNCHANGEbank;
    }

    public void setRUNCHANGEbank(String rUNCHANGEbank) {
        this.RUNCHANGEbank = rUNCHANGEbank;
    }

    public String getCommentaires() {
        return commentaires;
    }

    public void setCommentaires(String commentaires) {
        this.commentaires = commentaires;
    }

    public String getCommentairesPSO() {
        return commentairesPSO;
    }

    public void setCommentairesPSO(String commentairesPSO) {
        this.commentairesPSO = commentairesPSO;
    }

    public String getPiecesjointes() {
        return piecesjointes;
    }

    public void setPiecesjointes(String piecesjointes) {
        this.piecesjointes = piecesjointes;
    }

    @XmlTransient
    public List<Facture> getFactureList() {
        return factureList;
    }

    public void setFactureList(List<Facture> factureList) {
        this.factureList = factureList;
    }

    @XmlTransient
    public List<Intervention> getInterventionList() {
        return interventionList;
    }

    public void setInterventionList(List<Intervention> interventionList) {
        this.interventionList = interventionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reference != null ? reference.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contrat)) {
            return false;
        }
        Contrat other = (Contrat) object;
        if ((this.reference == null && other.reference != null) || (this.reference != null && !this.reference.equals(other.reference))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uib.Contrat[ reference=" + reference + " ]";
    }
    
}
