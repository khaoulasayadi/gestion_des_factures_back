/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Sana
 */
@Entity
@Table(name = "materiel")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Materiel.findAll", query = "SELECT m FROM Materiel m")
    , @NamedQuery(name = "Materiel.findByIdmat", query = "SELECT m FROM Materiel m WHERE m.idmat = :idmat")
    , @NamedQuery(name = "Materiel.findByNomMat", query = "SELECT m FROM Materiel m WHERE m.nomMat = :nomMat")
    , @NamedQuery(name = "Materiel.findByCout", query = "SELECT m FROM Materiel m WHERE m.cout = :cout")
    , @NamedQuery(name = "Materiel.findByBC", query = "SELECT m FROM Materiel m WHERE m.bcmFk.refBC = :bcmFK")
})
public class Materiel implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id_mat")
    private Integer idmat;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nom_mat")
    private String nomMat;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Cout")
    private float cout;
    @JoinColumn(name = "BCM_FK", referencedColumnName = "Ref_BC")
    @ManyToOne(optional = false)
    private BonCommande bcmFk;

    public Materiel() {
    }

    public Materiel(Integer idmat) {
        this.idmat = idmat;
    }

    public Materiel(Integer idmat, String nomMat, float cout) {
        this.idmat = idmat;
        this.nomMat = nomMat;
        this.cout = cout;
    }

    public Integer getIdmat() {
        return idmat;
    }

    public void setIdmat(Integer idmat) {
        this.idmat = idmat;
    }

    public String getNomMat() {
        return nomMat;
    }

    public void setNomMat(String nomMat) {
        this.nomMat = nomMat;
    }

    public float getCout() {
        return cout;
    }

    public void setCout(float cout) {
        this.cout = cout;
    }

    public BonCommande getBcmFk() {
        return bcmFk;
    }

    public void setBcmFk(BonCommande bcmFk) {
        this.bcmFk = bcmFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idmat != null ? idmat.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Materiel)) {
            return false;
        }
        Materiel other = (Materiel) object;
        if ((this.idmat == null && other.idmat != null) || (this.idmat != null && !this.idmat.equals(other.idmat))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Materiel[ idmat=" + idmat + " ]";
    }
    
}
