/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Sana
 */
@Entity
@Table(name = "bc_dsi_projet")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BcDsiProjet.findAll", query = "SELECT b FROM BcDsiProjet b")
    , @NamedQuery(name = "BcDsiProjet.findByRefBc", query = "SELECT b FROM BcDsiProjet b WHERE b.refBc = :refBc")
    , @NamedQuery(name = "BcDsiProjet.findByDatesignature", query = "SELECT b FROM BcDsiProjet b WHERE b.datesignature = :datesignature")
    , @NamedQuery(name = "BcDsiProjet.findByPrestataire", query = "SELECT b FROM BcDsiProjet b WHERE b.prestataire = :prestataire")
    , @NamedQuery(name = "BcDsiProjet.findByMnttttalHT", query = "SELECT b FROM BcDsiProjet b WHERE b.mnttttalHT = :mnttttalHT")
    , @NamedQuery(name = "BcDsiProjet.findByMnttttalTTC", query = "SELECT b FROM BcDsiProjet b WHERE b.mnttttalTTC = :mnttttalTTC")
    , @NamedQuery(name = "BcDsiProjet.findByFrais", query = "SELECT b FROM BcDsiProjet b WHERE b.frais = :frais")
    , @NamedQuery(name = "BcDsiProjet.findByTaxes", query = "SELECT b FROM BcDsiProjet b WHERE b.taxes = :taxes")
    , @NamedQuery(name = "BcDsiProjet.findByModaliteDePaiement", query = "SELECT b FROM BcDsiProjet b WHERE b.modaliteDePaiement = :modaliteDePaiement")
    , @NamedQuery(name = "BcDsiProjet.findByResponsable", query = "SELECT b FROM BcDsiProjet b WHERE b.responsable = :responsable")
    , @NamedQuery(name = "BcDsiProjet.findByCommentaire", query = "SELECT b FROM BcDsiProjet b WHERE b.commentaire = :commentaire")
    , @NamedQuery(name = "BcDsiProjet.findByPiecesjointes", query = "SELECT b FROM BcDsiProjet b WHERE b.piecesjointes = :piecesjointes")})
public class BcDsiProjet implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "RefBc")
    private Integer refBc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "Date_signature")
    private String datesignature;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Prestataire")
    private String prestataire;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Mntt_ttal_HT")
    private float mnttttalHT;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Mntt_ttal_TTC")
    private float mnttttalTTC;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Frais")
    private float frais;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Taxes")
    private float taxes;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "modalite_de_paiement")
    private String modaliteDePaiement;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Responsable")
    private String responsable;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "Commentaire")
    private String commentaire;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Pieces_jointes")
    private String piecesjointes;
    @JoinColumn(name = "Projet_FK", referencedColumnName = "Ref_projet")
    @ManyToOne(optional = false)
    private Projet projetFK;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dcpFk")
    private List<Facture> factureList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dsiiFK")  
    private List<Detail> detailList;

    public BcDsiProjet() {
    }

    public BcDsiProjet(Integer refBc) {
        this.refBc = refBc;
    }

    public BcDsiProjet(Integer refBc, String datesignature, String prestataire, float mnttttalHT, float mnttttalTTC, float frais, float taxes, String modaliteDePaiement, String responsable, String commentaire, String piecesjointes) {
        this.refBc = refBc;
        this.datesignature = datesignature;
        this.prestataire = prestataire;
        this.mnttttalHT = mnttttalHT;
        this.mnttttalTTC = mnttttalTTC;
        this.frais = frais;
        this.taxes = taxes;
        this.modaliteDePaiement = modaliteDePaiement;
        this.responsable = responsable;
        this.commentaire = commentaire;
        this.piecesjointes = piecesjointes;
    }

    public Integer getRefBc() {
        return refBc;
    }

    public void setRefBc(Integer refBc) {
        this.refBc = refBc;
    }

    public String getDatesignature() {
        return datesignature;
    }

    public void setDatesignature(String datesignature) {
        this.datesignature = datesignature;
    }

    public String getPrestataire() {
        return prestataire;
    }

    public void setPrestataire(String prestataire) {
        this.prestataire = prestataire;
    }

    public float getMnttttalHT() {
        return mnttttalHT;
    }

    public void setMnttttalHT(float mnttttalHT) {
        this.mnttttalHT = mnttttalHT;
    }

    public float getMnttttalTTC() {
        return mnttttalTTC;
    }

    public void setMnttttalTTC(float mnttttalTTC) {
        this.mnttttalTTC = mnttttalTTC;
    }

    public float getFrais() {
        return frais;
    }

    public void setFrais(float frais) {
        this.frais = frais;
    }

    public float getTaxes() {
        return taxes;
    }

    public void setTaxes(float taxes) {
        this.taxes = taxes;
    }

    public String getModaliteDePaiement() {
        return modaliteDePaiement;
    }

    public void setModaliteDePaiement(String modaliteDePaiement) {
        this.modaliteDePaiement = modaliteDePaiement;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public String getPiecesjointes() {
        return piecesjointes;
    }

    public void setPiecesjointes(String piecesjointes) {
        this.piecesjointes = piecesjointes;
    }

    public Projet getProjetFK() {
        return projetFK;
    }

    public void setProjetFK(Projet projetFK) {
        this.projetFK = projetFK;
    }

    @XmlTransient
    public List<Facture> getFactureList() {
        return factureList;
    }

    public void setFactureList(List<Facture> factureList) {
        this.factureList = factureList;
    }

    @XmlTransient
    public List<Detail> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<Detail> detailList) {
        this.detailList = detailList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refBc != null ? refBc.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BcDsiProjet)) {
            return false;
        }
        BcDsiProjet other = (BcDsiProjet) object;
        if ((this.refBc == null && other.refBc != null) || (this.refBc != null && !this.refBc.equals(other.refBc))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uib.BcDsiProjet[ refBc=" + refBc + " ]";
    }
    
}
