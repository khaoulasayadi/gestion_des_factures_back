/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Sana
 */
@javax.ws.rs.ApplicationPath("uib")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(services.BcDsiProjetFacadeREST.class);
        resources.add(services.BonCommandeFacadeREST.class);
        resources.add(services.BudgetFacadeREST.class);
        resources.add(services.ContratFacadeREST.class);
        resources.add(services.CorsFilter.class);
        resources.add(services.DetailFacadeREST.class);
        resources.add(services.DetailFactureFacadeREST.class);
        resources.add(services.FactureFacadeREST.class);
        resources.add(services.InterventionFacadeREST.class);
        resources.add(services.MaterielFacadeREST.class);
        resources.add(services.ProjetFacadeREST.class);
    }
    
}
